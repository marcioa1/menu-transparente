//
//  SubMenuViewController.swift
//  Menu
//
//  Created by Altran on 31/07/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import UIKit

class SubMenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var data = [ItemMenu]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.navigationBar.tintColor = .white
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "ItemMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "menuCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

   
}

extension SubMenuViewController: UITableViewDelegate{
    
}

extension SubMenuViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! ItemMenuTableViewCell
        if let item = data[indexPath.row] as? ItemMenu{
            cell.setup(item)
        }
        return cell
    }
    
    
}
