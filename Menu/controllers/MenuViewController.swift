
//
//  MenuViewController.swift
//  Menu
//
//  Created by Altran on 30/07/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import Foundation
import UIKit



class MenuViewController: UIViewController {
    
    @IBAction func teste(_ sender: UIButton) {
        print("clicou")
    }
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var secondaryTableView: UITableView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var realCloseButton: UIButton!
    
    @IBOutlet weak var topCloseButtonConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var leadingCloseButtonConstraint: NSLayoutConstraint!
    var menuData = Menu()
    let HIDE_MENU:CGFloat      = -390.0
    let LEADING_BUTTON:CGFloat = 300.0
    let TOP_CLOSE_BUTTON:CGFloat = 44.0
    var selectedHeader:Int = -1
    var selectedChild : ItemMenu? // = ItemMenu()
    var actionToPerform: (() -> ()) =  {}
    var previousConstraitnt:CGFloat = 44.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuData.menuVC = self
        menuData.build()
        setupTableView()
        addGestures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.menuView.frame.origin.x = HIDE_MENU
        self.transparentView.frame.origin.x = HIDE_MENU
        if self.view.frame.width == 320.0 {
            self.leadingCloseButtonConstraint.constant = 284.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    fileprivate func openMenu() {
        mainTableView.setContentOffset(.zero, animated: true)
        topCloseButtonConstraint.constant =  TOP_CLOSE_BUTTON
        UIView.animate(withDuration: 0.3){
            self.menuView.frame.origin.x = 0
            self.closeButton.frame.origin.x = self.LEADING_BUTTON
            self.realCloseButton.alpha = 1.0
            self.transparentView.frame.origin.x = 0
            self.mainTableView.frame.origin.x = 0.0
            self.secondaryTableView.alpha = 0.0
            self.mainTableView.reloadData()
            self.selectedChild = nil
        }
    }
    
    @IBAction func openPressed(_ sender: UIButton) {
        openMenu()
    }
    
    fileprivate func closeMenu() {
        UIView.animate(withDuration: 0.3){
            self.menuView.frame.origin.x = self.HIDE_MENU
            self.transparentView.frame.origin.x = self.HIDE_MENU
            self.secondaryTableView.alpha = 0.0
            self.realCloseButton.alpha = 0.0
        }
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        closeMenu()
    }
    
    fileprivate func setupTableView(){
        mainTableView.register(UINib(nibName: "ItemMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "mainCell")
        secondaryTableView.register(UINib(nibName: "ItemMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "mainCell")
        let headerNib = UINib.init(nibName: "HeaderMenuView", bundle: Bundle.main)
        mainTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "headerCell")
        mainTableView.tableHeaderView      = userHeaderView()
        mainTableView.tableFooterView      = menuFooterView()
        secondaryTableView.tableHeaderView = navigationHeader()
        secondaryTableView.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mainTableView.reloadData()
    }
    
    fileprivate func userHeaderView() -> UIView {
        return (Bundle.main.loadNibNamed("UserHeaderView", owner: self, options: nil)![0] as! UIView)
    }
    
    fileprivate func menuFooterView() -> UIView{
        return (Bundle.main.loadNibNamed("FooterMenuView", owner: self, options: nil)![0] as! UIView)
    }
    
    func navigationHeader() -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.secondaryTableView.frame.width, height: 63))
        view.backgroundColor = UIColor(red: 37/255, green: 37/255, blue: 37/255, alpha: 1.0)
        let backButton = UIButton(frame: CGRect(x: 16, y: 24, width: 32, height: 32))
        backButton.setImage(UIImage(named: "voltar"), for: .normal)
        backButton.addTarget(self, action: #selector(closeSecondaryMenu(_:)), for: .touchUpInside)
        view.addSubview(backButton)
        let backLabel = UILabel(frame: CGRect(x: 52, y: 27, width: 49, height: 24))
        backLabel.text = "Voltar"
        backLabel.font = UIFont(name: "Montserrat-Regular", size: 16)
        backLabel.textColor = .white
        view.addSubview(backLabel)
        return view
    }
    
    @objc func closeSecondaryMenu(_ sender: UIButton!){
        UIView.animate(withDuration: 0.5){
            self.mainTableView.frame.origin.x = 0
            self.secondaryTableView.alpha = 0.0
            self.secondaryTableView.frame.origin.x = 320
            self.closeButton.frame.origin.x = self.LEADING_BUTTON
            self.topCloseButtonConstraint.constant = self.previousConstraitnt
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.destination)
    }
    
    fileprivate func openSecondaryTableView() {
        UIView.animate(withDuration: 0.5){
            self.mainTableView.frame.origin.x = self.HIDE_MENU
            self.secondaryTableView.alpha = 1.0
            self.secondaryTableView.frame.origin.x = 0
            self.closeButton.frame.origin.x   = self.HIDE_MENU
            self.topCloseButtonConstraint.constant = self.TOP_CLOSE_BUTTON
        }
    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == mainTableView {
            let group = menuData.items[indexPath.section]
            selectedChild = group.children[indexPath.row]
            if selectedChild!.available {
                let item = group.children[indexPath.row] as? ItemMenu
                if item != nil && item!.isLeaf {
                    self.closeMenu()
                    item!.action()
                }else {
                    selectedChild = item!
                    secondaryTableView.reloadData()
                    openSecondaryTableView()
                }
            }
        }else {
            if let item = selectedChild!.children[indexPath.row] as? ItemMenu,
                item.isLeaf {
                item.action()

            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == mainTableView {
            let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                "headerCell") as! HeaderMenuView
            headerCell.setup(itemMenu: menuData.items[section], section: section)
            headerCell.delegate = self
            headerCell.tag = section
            return headerCell
        }else {
            let header = Bundle.main.loadNibNamed("SecondaryHeaderView", owner: self, options: nil)![0] as! SecondaryHeaderView
            header.titleLabel.text = selectedChild?.title
            return header
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        topCloseButtonConstraint.constant = (scrollView.contentOffset.y * -1) + TOP_CLOSE_BUTTON
        previousConstraitnt = topCloseButtonConstraint.constant
    }
    
    
}

extension MenuViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == mainTableView {
            return menuData.items.count
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainTableView{
            let headerItem = menuData.items[section]
            return headerItem.numberOfRows
        }else {
            return selectedChild?.numberOfRows ?? 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let group = menuData.items[indexPath.section]
        if tableView == mainTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath) as! ItemMenuTableViewCell
            if let item = group.children[indexPath.row] as? ItemMenu {
                cell.setup(item)
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath) as! ItemMenuTableViewCell
            cell.titleLabel.text = selectedChild?.children[indexPath.row].title
            if let item = selectedChild?.children[indexPath.row] as? ItemMenu {
                cell.setup(item)
            }
            return cell
        }
    }
}

extension MenuViewController{
    
    func addGestures(){
        let leftRecognizer = UISwipeGestureRecognizer(target: self, action:
            #selector(swipeMade(_:)))
        leftRecognizer.direction = .left
        let rightRecognizer = UISwipeGestureRecognizer(target: self, action:
            #selector(swipeMade(_:)))
        rightRecognizer.direction = .right
        self.view.addGestureRecognizer(leftRecognizer)
        self.view.addGestureRecognizer(rightRecognizer)
    }
    @IBAction func swipeMade(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .left {
            closeMenu()
        }else if sender.direction == .right {
            openMenu()
        }
    }
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        menuData.items[section].collapsed.toggle()
        mainTableView.beginUpdates()
        mainTableView.reloadSections(IndexSet(integer: section), with: .none)
        mainTableView.endUpdates()
        //        if  !menuData.items[section].collapsed {
        //            let indexPath = IndexPath(row: menuData.items[section].children.count-1, section: section)
        //            mainTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        //        }
    }
}

extension MenuViewController: HeaderViewDelegate{
    func didTapButtonInCell(_ cell: HeaderMenuView) {
        mainTableView.beginUpdates()
        cell.itemMenu?.collapsed.toggle()
        mainTableView.reloadSections(IndexSet(integer: cell.section), with: .none)
        mainTableView.endUpdates()
    }
    
    func toggleSection(header: HeaderMenuView, section: Int) {
        print("toggle")
    }
    
    
}
