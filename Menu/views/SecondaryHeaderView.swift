//
//  SecondaryHeaderView.swift
//  Menu
//
//  Created by Altran on 04/08/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import UIKit

class SecondaryHeaderView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    
}
