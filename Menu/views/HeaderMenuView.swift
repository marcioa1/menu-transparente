//
//  HeaderMenuView.swift
//  Menu
//
//  Created by Altran on 30/07/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: class {
    func didTapButtonInCell(_ cell: HeaderMenuView)
    func toggleSection(header: HeaderMenuView, section: Int)
}

class HeaderMenuView : UITableViewHeaderFooterView {
    
    @IBOutlet weak var sectionBackgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var orangeImage: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var expandButton: UIImageView!
    
    var itemMenu:ItemMenu?
    var section: Int = 0
    weak var delegate: HeaderViewDelegate?
    
    var reloadSections: ((_ section: Int) -> Void)?
    
    func setup(itemMenu: ItemMenu, section: Int){
        self.itemMenu   = itemMenu
        self.section    = section
        titleLabel.text = itemMenu.title
        iconImage.image = itemMenu.icon
        if section < 2 {
            sectionBackgroundView.backgroundColor = .white
        }else {
            sectionBackgroundView.backgroundColor = UIColor.init(red: 244/255, green: 245/255, blue: 246/255, alpha: 1.0)
        }
        if itemMenu.collapsible{
            expandButton.isHidden = false
            setCollapsed()
        }else {
            expandButton.isHidden = true
        }
    }
    
    func setCollapsed() {
        let collapsed = self.itemMenu!.collapsed
        if collapsed{
            expandButton?.image = UIImage(named: "expand")
        }else {
            expandButton?.image = UIImage(named: "collapse")
            
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func didTapButton(sender: UIButton) {
           delegate?.didTapButtonInCell(self)
       }
    func toggleSection(header: HeaderMenuView, section: Int) {
        //        setCollapsed(collapsed: self.itemMenu!.collapsed)
    }
    
    
}

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.3) {
        //        print("rotation: \(section)")
        print("rotate")
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        self.layer.add(animation, forKey: nil)
    }
}
