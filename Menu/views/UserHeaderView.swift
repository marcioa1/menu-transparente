//
//  UserHeaderTableViewCell.swift
//  Menu
//
//  Created by Altran on 30/07/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import UIKit

class UserHeaderView: UIView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var agencyNumberLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var changeDataButton: UIButton!
    @IBOutlet weak var avatarImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }
    
    let  user = FakeUser()
    
    @IBAction func compartilharPressed(_ sender: UIButton) {
    }
    
    
    @IBAction func trocarContaPressed(_ sender: UIButton) {
    }
    
    @IBAction func alterarDadosPressed(_ sender: UIButton) {
    }
    
    fileprivate func setupUserData() {
        nameLabel.text          = user.name
        agencyNumberLabel.text  = user.agencyNumber
        accountNumberLabel.text = user.accountNumber
    }
    
    func setupView(){
        avatarImage.layer.borderWidth   = 2
        avatarImage.layer.masksToBounds = false
        avatarImage.layer.borderColor   = UIColor.white.cgColor
        avatarImage.layer.cornerRadius  = avatarImage.frame.width/2.0
        avatarImage.layer.masksToBounds = true
        
        let attributeButton: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "Montserrat-Regular", size: 14.0)! ,
            .foregroundColor: UIColor.white,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Alterar dados pessoais", attributes: attributeButton)
        changeDataButton.setAttributedTitle(attributeString, for: .normal)
        
        setupUserData()
    }


    
}
