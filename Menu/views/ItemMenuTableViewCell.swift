//
//  ItemMenuTableViewCell.swift
//  Menu
//
//  Created by Altran on 30/07/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import UIKit

class ItemMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var availableImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setup(_ item: ItemMenu){
        titleLabel.text = item.title
        availableImage.isHidden =
            item.available
        if item.available{
        self.accessoryType = .disclosureIndicator
        }else {
            self.accessoryType = .none
        }
    }
}
