//
//  FooterMenuView.swift
//  Menu
//
//  Created by Altran on 05/08/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import UIKit
import StoreKit

class FooterMenuView: UIView {
    
    @IBOutlet weak var evaluateButton: UIButton!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBAction func evaluatePressed(_ sender: UIButton) {
        SKStoreReviewController.requestReview()
    }
    
    override func draw(_ rect: CGRect) {
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            let appBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                versionLabel.text = "versão \(appVersion).\(appBuild)"
            }else {
            versionLabel.text = ""
        }
        
        let attributeButton: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "Montserrat-Regular", size: 16.0)! ,
            .foregroundColor: UIColor.init(red: 70/255, green: 42/255, blue: 113/255, alpha: 1.0),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let evaluateAttributeString = NSMutableAttributedString(string: "Avalie nosso app", attributes: attributeButton)
        let exitAttributeString = NSMutableAttributedString(string: "Sair do aplicativo", attributes: attributeButton)
        evaluateButton.setAttributedTitle(evaluateAttributeString, for: .normal)
        exitButton.setAttributedTitle(exitAttributeString, for: .normal)
    }
}

