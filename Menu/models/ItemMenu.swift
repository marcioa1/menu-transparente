//
//  ItemMenu.swift
//  Menu
//
//  Created by Altran on 30/07/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import Foundation
import UIKit


class ItemMenu{
    var header:Bool = false
    var icon: UIImage?
    var title:String = ""
    var collapsible:Bool = false
    var children: [ItemMenu] = []
    var available = true
    var collapsed:Bool = false
    var action: (() -> ()) = {}
    var isLeaf:Bool{
        get{
            children.isEmpty == true
        }
    }
    
    var numberOfRows:Int{
        get{
            if self.collapsible && self.collapsed{
                return  0
            }
            return self.children.count
        }
    }
    
    init(header: Bool , icon: UIImage?, title: String, collapsible: Bool, collapsed: Bool = false, children: [ItemMenu], available: Bool, action:@escaping (() -> ())){
        self.header = header
        self.icon = icon
        self.title = title
        self.collapsible = collapsible
        self.collapsed = collapsed
        self.children = children
        self.available = available
        self.action = action
    }
}


