//
//  Menu.swift
//  Menu
//
//  Created by Altran on 30/07/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import Foundation
import UIKit

class Menu {
    var items = [ItemMenu]()
    weak var menuVC:MenuViewController?

 
    func build(){
        
        let itemVerExtrato = ItemMenu(header: false, icon: UIImage(), title: "Ver extrato", collapsible: false, children: [], available: true, action: {self.menuVC?.performSegue(withIdentifier: "goViaSegue", sender: self.menuVC)})
        let itemPagarConta = ItemMenu(header: false, icon: UIImage(), title: "Pagar conta", collapsible: false, children: [], available: true, action: {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "fakeVC")
            self.menuVC?.present(vc, animated: true, completion: nil)
        })
        let itemFazerTansferencia = ItemMenu(header: false, icon: nil, title: "Fazer transferência", collapsible: false, children: [], available: true, action: {})
        let itemSacarDinheiro = ItemMenu(header: false, icon: nil, title: "Sacar dinheiro", collapsible: false, children: [], available: true, action: {})
        let itemVerComprovantes = ItemMenu(header: false, icon: nil, title: "Ver comprovantes", collapsible: false, children: [], available: true, action: {})
        let itemVerAgendamentos = ItemMenu(header: false, icon: nil, title: "Ver agendamentos", collapsible: false, children: [], available: true, action: {})
        let header = ItemMenu(header: true, icon: UIImage(named: "acesso-rapido"), title: "Acesso Rápido", collapsible: false, children: [itemVerExtrato, itemPagarConta,itemFazerTansferencia, itemSacarDinheiro, itemVerComprovantes, itemVerAgendamentos], available: true, action: {})
        
        let minhaContaPagar = ItemMenu(header: false, icon: nil, title: "Pagar conta", collapsible: false, children: [], available: true, action: {})
        let minhaContaTransferencia = ItemMenu(header: false, icon: nil, title: "Fazer transferência", collapsible: false, children: [], available: true, action: {})
        let minhaContaSacar = ItemMenu(header: false, icon: nil, title: "Sacar dinheiro", collapsible: false, children: [], available: true, action: {})
        let minhaContaComprovantes = ItemMenu(header: false, icon: nil, title: "Ver comprovantes", collapsible: true, children: [], available: false, action: {})
        let minhaContaAgendamento = ItemMenu(header: false, icon: nil, title: "Ver agendamentos", collapsible: true, children: [], available: false, action: {})
        let minhaContaDebito = ItemMenu(header: false, icon: nil, title: "Débito automático", collapsible: false, children: [], available: false, action: {})
        let minhaContaSalarios = ItemMenu(header: false, icon: nil, title: "Pagar salários no BMG", collapsible: false, children: [], available: false, action: { print("conta salario") })
        let minhaContaBeneficios = ItemMenu(header: false, icon: nil, title: "Plano de benefícios", collapsible: false, children: [], available: false, action: {})
        let minhaContaServicos = ItemMenu(header: false, icon: nil, title: "Ver cesta de serviços", collapsible: false, children: [], available: false, action: {})
        
        let minhaConta = ItemMenu(header: false, icon: nil, title: "Minha conta", collapsible: false, children: [minhaContaPagar, minhaContaTransferencia, minhaContaSacar, minhaContaComprovantes, minhaContaAgendamento, minhaContaDebito, minhaContaSalarios,minhaContaBeneficios, minhaContaServicos], available: true, action: {})
        
        
        let administrarCartoes = ItemMenu(header: false, icon: nil, title: "Administrar cartões", collapsible: false, children: [], available: true, action: {})
        let configuracoesDeGrupos = ItemMenu(header: false, icon: nil, title: "Configurações de grupos", collapsible: false, children: [], available: true, action: {})
        let configuracoesDeLimites = ItemMenu(header: false, icon: nil, title: "Configurações de limites", collapsible: false, children: [], available: true, action: {})
        let autorizarSolicitacoes = ItemMenu(header: false, icon: nil, title: "Autorizar solicitações", collapsible: true, children: [], available: true, action: {})
        let historicoDeCompras = ItemMenu(header: false, icon: nil, title: "Histórico de comoras", collapsible: true, children: [], available: true, action: {})
        let alertasENotificacoes = ItemMenu(header: false, icon: nil, title: "Alertas e notificações", collapsible: false, children: [], available: true, action: {})
        let bloquiosECancelamentos = ItemMenu(header: false, icon: nil, title: "Bloqueios e cancelamentos", collapsible: false, children: [], available: true, action: { print("conta salario") })
        let outrosServicos = ItemMenu(header: false, icon: nil, title: "Outros serviços", collapsible: false, children: [], available: true, action: {})

        let cartoes = ItemMenu(header: false, icon: nil, title: "Cartões", collapsible: false, children: [administrarCartoes, configuracoesDeGrupos, configuracoesDeLimites, autorizarSolicitacoes, historicoDeCompras, alertasENotificacoes, bloquiosECancelamentos, outrosServicos ], available: true, action: {})
        let emprestimos = ItemMenu(header: false, icon: nil, title: "Empréstimos", collapsible: false, children: [], available: false, action: {})
        let empresas = ItemMenu(header: true, icon: UIImage(named: "e-pra-mim"), title: "Para empresas", collapsible: false, children: [minhaConta, cartoes, emprestimos], available: false, action: {})
        
        
        let deConta = ItemMenu(header: false, icon: nil, title: "Configurações de conta", collapsible: false, children: [], available: true, action: {})
        let doCartao = ItemMenu(header: false, icon: nil, title: "Configurações do cartão", collapsible: false, children: [], available: false, action: {})
        let administrar = ItemMenu(header: false, icon: nil, title: "Administrar meus dados", collapsible: false, children: [], available: true, action: {})
        let biometria = ItemMenu(header: false, icon: nil, title: "Configurar biometria", collapsible: false, children: [], available: true, action: {})
        let encerramento = ItemMenu(header: false, icon: nil, title: "Encerramento de conta", collapsible: false, children: [], available: true, action: {})
        let configuracoes = ItemMenu(header: true, icon: UIImage(named: "minhas-configuracoes"), title: "Minhas configurações", collapsible: true, collapsed: true, children: [deConta, doCartao, administrar, biometria, encerramento], available: true,  action: {})
        
        let faleConosco = ItemMenu(header: false, icon: nil, title: "Fale conosco", collapsible: false, children: [], available: true, action: {})
        let perguntasFrequentes = ItemMenu(header: false, icon: nil, title: "Perguntas frequentes", collapsible: false, children: [], available: true, action: {})
        let ajuda = ItemMenu(header: true, icon: UIImage(named: "ajuda"), title: "Ajuda", collapsible: true, collapsed: true, children: [faleConosco, perguntasFrequentes], available: true,  action: {})
        
        let canaisDigitais = ItemMenu(header: false, icon: nil, title: "Configurações de conta", collapsible: false, children: [], available: true, action: {})
        let centralDeRelacionamento = ItemMenu(header: false, icon: nil, title: "Configurações do cartão", collapsible: false, children: [], available: true, action: {})
        let deficientes = ItemMenu(header: false, icon: nil, title: "Administrar meus dados", collapsible: false, children: [], available: true, action: {})
        let ouvidoria = ItemMenu(header: false, icon: nil, title: "Configurar biometria", collapsible: false, children: [], available: true, action: {})
        let atendimento = ItemMenu(header: true, icon: UIImage(named: "atendimento"), title: "Atendimento", collapsible: true, collapsed: true, children: [canaisDigitais, centralDeRelacionamento, deficientes, ouvidoria], available: true,  action: {})
        
        let termoDeAdesao = ItemMenu(header: false, icon: nil, title: "Fale conosco", collapsible: false, children: [], available: true, action: {})
        let termoDeCredenciamento = ItemMenu(header: false, icon: nil, title: "Fale conosco", collapsible: false, children: [], available: true, action: {})
        let termoDePrivacidade = ItemMenu(header: false, icon: nil, title: "Perguntas frequentes", collapsible: false, children: [], available: true, action: {})
        let informes = ItemMenu(header: true, icon: UIImage(named: "informes"), title: "Informes", collapsible: true, collapsed: true, children: [termoDeAdesao, termoDeCredenciamento, termoDePrivacidade], available: true, action: {})
        
        items =  [header, empresas, configuracoes, ajuda, atendimento, informes]
        
    }
    
}
