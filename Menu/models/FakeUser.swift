//
//  FakeUser.swift
//  Menu
//
//  Created by Altran on 05/08/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import Foundation

struct FakeUser {
    let name = "Rogério da Silva"
    let agencyNumber = "0044"
    let accountNumber = "5446345-7"
}
