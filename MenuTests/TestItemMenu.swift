//
//  TestItemMenu.swift
//  MenuTests
//
//  Created by Altran on 01/08/20.
//  Copyright © 2020 Altran. All rights reserved.
//

import XCTest
@testable import Menu

class FakeItemMenu {
    static func getInstance() -> ItemMenu {
        return ItemMenu(header: false, icon: nil, title: "Teste", collapsible: true, children: [], available: true, action: {}, actionParameter: nil)
    }
}

class TestItemMenu: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testItemIsLeaf() {
        let item = FakeItemMenu.getInstance() //  ItemMenu(children: [], action: {})
        XCTAssertTrue(item.isLeaf)
    }
    func testItemIsNotLeafIfItHasChildren() {
        let child = FakeItemMenu.getInstance() // ItemMenu(action: {})
        let item = FakeItemMenu.getInstance()
        item.children = [child]
        XCTAssertFalse(item.isLeaf)
    }
    
    func testIfNumberOfRowsIsZeroWhenHasNoChildren() {
        let item = FakeItemMenu.getInstance()
        XCTAssertEqual(item.numberOfRows, 0)
    }

    func testIfNumberOfRowsIsNotZeroWhenItemIsNotCollapsible() {
        let child = FakeItemMenu.getInstance()
        var item = FakeItemMenu.getInstance() //ItemMenu(collapsible: false,  children: [child], action: {})
        item.children = [child]
        item.collapsible = false
        item.collapsed = true
        XCTAssertEqual(item.numberOfRows, 1)
    }
    
    func testIfNumberOfRowsIsZeroWhenItemHasChildrenAndIsCollapsed() {
        let child = FakeItemMenu.getInstance()
        var item = FakeItemMenu.getInstance() //ItemMenu(collapsible: true,  children: [child], action: {})
        item.children = [child]
        item.collapsible = true
        item.collapsed = true
        XCTAssertEqual(item.numberOfRows, 0)
    }

}
